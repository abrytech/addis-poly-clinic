import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import compression from 'compression'
import { urlencoded, json } from 'body-parser'
import authRouter from './router/auth_router'
import autoRouter from './router/auto_router'
import apiRouter from './router/index'
import { authUser } from './middleware/auth'
const app = express()

const port = process.env.PORT
const www = process.env.WWW || './public'
app.use(compression())
app.use(helmet())
app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204
}))
app.use(urlencoded({ extended: false }))
app.use(json())
app.use(express.static(www))

app.use('/auto', autoRouter)
app.use('/auth', authRouter)
app.use('/api', authUser, apiRouter)
app.use((req, res, next) => {
  const error = new Error('Resource not found')
  error.status = 404
  next(error)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
