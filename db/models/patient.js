'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Patient extends Model {
    static associate (models) {
      // define association here
      Patient.hasMany(models.Visitor, { foreignKey: 'patientId', as: 'visits' })
      Patient.hasMany(models.Lab, { foreignKey: 'patientId', as: 'labs' })
      Patient.hasMany(models.Appointment, { foreignKey: 'patientId', as: 'appointments' })
      Patient.hasOne(models.Shelf, { foreignKey: 'patientId', as: 'shelf' })
      Patient.belongsTo(models.User, { foreignKey: 'createdBy', as: 'creator' })
      Patient.belongsTo(models.User, { foreignKey: 'updatedBy', as: 'updater' })
    }
  };
  Patient.init({
    firstName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    fatherName: {
      type: DataTypes.STRING
    },
    grandName: DataTypes.STRING,
    cardNumber: {
      unique: true,
      type: DataTypes.STRING
    },
    gender: {
      type: DataTypes.STRING,
      validate: {
        isIn: [['FEMALE', 'Female', 'female', 'F', 'MALE', 'Male', 'male', 'M']]
      }
    },
    age: {
      type: DataTypes.INTEGER,
      validate: {
        max: 150, // only allow values <= 23
        min: 0 // only allow values >= 23
      }
    },
    phone: {
      type: DataTypes.STRING,
      validate: {
        len: [10, 16]
      }
    },
    kebele: DataTypes.INTEGER,
    woreda: DataTypes.STRING,
    subCity: DataTypes.STRING,
    houseNo: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Patient'
  })
  return Patient
}
