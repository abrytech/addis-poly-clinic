'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LabResult extends Model {
    static associate (models) {
      // define association here
      LabResult.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' })
      LabResult.belongsTo(models.TestType, { foreignKey: 'testTypeId', as: 'testType' })
      LabResult.belongsTo(models.User, { foreignKey: 'createdBy', as: 'laboratorist' })
      LabResult.belongsTo(models.User, { foreignKey: 'updatedBy', as: 'updater' })
    }
  };
  LabResult.init({
    patientId: DataTypes.INTEGER,
    testTypeId: DataTypes.INTEGER,
    labRequestId: DataTypes.INTEGER,
    result: DataTypes.STRING,
    remark: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'LabResult'
  })
  return LabResult
}
