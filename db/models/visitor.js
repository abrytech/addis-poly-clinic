'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Visitor extends Model {
    static associate (models) {
      // define association here
      Visitor.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' })
    }
  };
  Visitor.init({
    patientId: DataTypes.INTEGER,
    date: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Visitor'
  })
  return Visitor
}
