'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Appointment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      Appointment.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' })
      Appointment.belongsTo(models.User, { foreignKey: 'doctorId', as: 'assignedDoctor' })
      Appointment.belongsTo(models.User, { foreignKey: 'createdBy', as: 'creator' })
      Appointment.belongsTo(models.User, { foreignKey: 'updatedBy', as: 'updater' })
    }
  };
  Appointment.init({
    patientId: DataTypes.INTEGER,
    doctorId: DataTypes.INTEGER,
    remark: DataTypes.STRING,
    appointmentDate: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, { sequelize, modelName: 'Appointment' })
  return Appointment
}
