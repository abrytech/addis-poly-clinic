'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Lab extends Model {
    static associate (models) {
      // define association here
      Lab.belongsTo(models.User, { foreignKey: 'physicianId', as: 'physician' })
      Lab.belongsTo(models.User, { foreignKey: 'laboratoristId', as: 'laboratorist' })
      Lab.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' })
    }
  };
  Lab.init({
    examInfo: DataTypes.STRING(1000),
    physicianId: DataTypes.INTEGER,
    labInfo: DataTypes.STRING(1000),
    laboratoristId: DataTypes.INTEGER,
    patientId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Lab'
  })
  return Lab
}
