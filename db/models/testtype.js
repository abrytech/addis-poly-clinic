'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class TestType extends Model {
    static associate (models) {
      // define association here
      TestType.belongsTo(models.TestCategory, { foreignKey: 'testCategoryId', as: 'testCategory' })
    }
  };
  TestType.init({
    name: DataTypes.STRING,
    testCategoryId: DataTypes.INTEGER,
    normalValue: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'TestType'
  })
  return TestType
}
