'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Shelf extends Model {
    static associate (models) {
      // define association here
      Shelf.belongsToMany(models.Patient, { through: 'PatientShelf' })
    }
  };
  Shelf.init({
    name: DataTypes.STRING,
    column: DataTypes.INTEGER,
    row: DataTypes.INTEGER,
    remark: DataTypes.STRING(1000),
    patientId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Shelf'
  })
  return Shelf
}
