'use strict'
const { Model } = require('sequelize')
const { hashSync, genSaltSync } = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate (models) {
      // define association here
      User.hasMany(models.Appointment, { foreignKey: 'doctorId', as: 'appointments' })
      User.hasMany(models.Patient, { foreignKey: 'createdBy', as: 'createdPatients' })
      User.hasMany(models.Patient, { foreignKey: 'updatedBy', as: 'updatedPatients' })
    }
  };
  User.init({
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        len: [10, 16]
      }
    },
    isActive: DataTypes.BOOLEAN,
    role: {
      type: DataTypes.STRING,
      validate: {
        isIn: [['Admin', 'Lab', 'Doctor', 'Reception']]
      },
      allowNull: false
    }
  }, {
    hooks: {
      beforeCreate: (user, option) => {
        if (user.password) {
          user.password = hashSync(user.password, genSaltSync(8), null)
        }
      },
      beforeUpdate: (user, options) => {
        if (user.password) {
          user.password = hashSync(user.password, genSaltSync(8), null)
        }
      }
    },
    sequelize,
    modelName: 'User'
  })
  return User
}
