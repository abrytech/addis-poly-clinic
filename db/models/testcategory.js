'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class TestCategory extends Model {
    static associate (models) {
      // define association here
    }
  };
  TestCategory.init({
    name: DataTypes.STRING,
    remark: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'TestCategory'
  })
  return TestCategory
}
