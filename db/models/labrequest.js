'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LabRequest extends Model {
    static associate (models) {
      // define association here
      LabRequest.belongsTo(models.Patient, { foreignKey: 'patientId', as: 'patient' })
      LabRequest.belongsTo(models.User, { foreignKey: 'createdBy', as: 'physician' })
      LabRequest.belongsTo(models.User, { foreignKey: 'updatedBy', as: 'updater' })
    }
  };
  LabRequest.init({
    patientId: DataTypes.INTEGER,
    testTypesId: DataTypes.STRING,
    status: {
      type: DataTypes.STRING,
      defaultValue: 'New',
      validate: {
        isIn: [['New', 'InProgress', 'Done']]
      }
    },
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'LabRequest'
  })
  return LabRequest
}
