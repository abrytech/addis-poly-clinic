'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Patients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      fatherName: {
        type: Sequelize.STRING
      },
      grandName: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      cardNumber: {
        unique: true,
        type: Sequelize.STRING
      },
      age: {
        type: Sequelize.INTEGER
      },
      phone: {
        type: Sequelize.STRING
      },
      kebele: {
        type: Sequelize.INTEGER
      },
      woreda: {
        type: Sequelize.STRING
      },
      subCity: {
        type: Sequelize.STRING
      },
      houseNo: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.INTEGER
      },
      updatedBy: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Patients')
  }
}
