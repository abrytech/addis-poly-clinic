import { Router } from 'express'
import { Op } from 'sequelize'
import _ from 'lodash'
import { LabRequest, Patient, User, TestType, TestCategory } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()
const include = [{ model: Patient, as: 'patient' }, { model: User, as: 'physician' }, { model: User, as: 'updater' }]
router.get('/:id(\\d+)', async (req, res) => {
  try {
    const where = { id: req.params.id }
    const labRequest = await LabRequest.findOne({ where, include })
    const row = labRequest.dataValues
    if (row.testTypesId) {
      const ids = row.testTypesId.split(',').map((sub) => parseInt(sub.trim()))
      const types = await TestType.findAll({ where: { id: { [Op.in]: ids } }, include: { model: TestCategory, as: 'testCategory' } })
      const uniqueCats = _.uniqBy(types, 'testCategoryId').map((_type) => _type.testCategory.dataValues)
      const cats = []
      for (const cat of uniqueCats) {
        cat.testTypes = []
        for (const inItem of types) {
          const item = inItem.dataValues
          delete item.testCategory
          if (cat.id == item.testCategoryId) {
            cat.testTypes.push(item)
          }
        }
        cats.push(cat)
      }
      row.testCategories = cats
      console.log('row.testCategories: ', row.testCategories)
    }
    if (row) res.send(row)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry LabRequest Not Found', stack: '' })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('', async (req, res, next) => {
  try {
    const body = req.body
    body.createdBy = req.userId
    LabRequest.create(body).then(async (labRequest) => {
      const _labRequest = await LabRequest.findOne({ where: { id: labRequest.id }, include }).catch((error) => {
        res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send(_labRequest)
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _labRequest = await LabRequest.findOne({ where: { id: body.id } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_labRequest) {
      body.patientId = body.patientId || _labRequest.patientId
      body.testTypesId = body.testTypesId || _labRequest.testTypesId
      body.labRequestInfo = body.labRequestInfo || _labRequest.labRequestInfo
      body.updateBy = req.userId
      const rows = await LabRequest.update(body, { where: { id: body.id } })
      const labRequest = await LabRequest.findOne({ where: { id: body.id }, include }).catch((error) => {
        res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
      })
      const row = labRequest.dataValues
      if (row.testTypesId) {
        const ids = row.testTypesId.split(',').map((sub) => parseInt(sub.trim()))
        const types = await TestType.findAll({ where: { id: { [Op.in]: ids } }, include: { model: TestCategory, as: 'testCategory' } })
        const uniqueCats = _.uniqBy(types, 'testCategoryId').map((_type) => _type.testCategory.dataValues)
        const cats = []
        for (const cat of uniqueCats) {
          cat.testTypes = []
          for (const inItem of types) {
            const item = inItem.dataValues
            delete item.testCategory
            if (cat.id == item.testCategoryId) {
              cat.testTypes.push(item)
            }
          }
          cats.push(cat)
        }
        row.testCategories = cats
        console.log('row.testCategories: ', row.testCategories)
      }
      if (row) res.send({ rows: rows.length, result: row })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the labRequest', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing labRequest id', stack: '' })
})

router.get('', async (req, res) => {
  try {
    const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
    const totalCount = await LabRequest.count()
    const rows = await LabRequest.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    const someProcedure = async n => {
      const _rows = []
      for (let i = 0; i < n; i++) {
        const row = rows[i].dataValues
        if (row.testTypesId) {
          const ids = row.testTypesId.split(',').map((sub) => parseInt(sub.trim()))
          console.log('ids:', ids)
          const types = await TestType.findAll({ where: { id: { [Op.in]: ids } } })
          row.testTypes = types.map((typ) => typ.dataValues)
          _rows.push(row)
        }
      }
      return _rows
    }
    someProcedure(rows.length).then((_rows) => {
      res.send({ labRequests: _rows, page: params.page, count: rows.length, total: totalCount })
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.get('/:query', getParams, async (req, res) => {
  try {
    const params = req.queries
    const totalCount = await LabRequest.count()
    const rows = await LabRequest.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    const someProcedure = async n => {
      const _rows = []
      for (let i = 0; i < n; i++) {
        const row = rows[i].dataValues
        if (row.testTypesId) {
          const ids = row.testTypesId.split(',').map((sub) => parseInt(sub.trim()))
          console.log('ids:', ids)
          const types = await TestType.findAll({ where: { id: { [Op.in]: ids } } })
          row.testTypes = types.map((typ) => typ.dataValues)
          _rows.push(row)
        }
      }
      return _rows
    }
    someProcedure(rows.length).then((_rows) => {
      res.send({ labRequests: _rows, page: params.page, count: rows.length, total: totalCount })
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

export default router
