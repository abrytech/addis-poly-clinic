import { Router } from 'express'
import { TestCategory } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()

router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  TestCategory.findOne({ where }).then((testCategory) => {
    if (testCategory) res.send(testCategory)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry TestCategory Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  TestCategory.create(body).then(async (testCategory) => {
    const _testCategory = await TestCategory.findOne({ where: { id: testCategory.id } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_testCategory)
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _testCategory = await TestCategory.findOne({ where: { id: body.id } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_testCategory) {
      body.name = body.name || _testCategory.name
      body.remark = body.remark || _testCategory.remark
      const rows = await TestCategory.update(body, { where: { id: body.id } })
      body.patient = _testCategory.patient
      res.send({ rows, result: body })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the testCategory', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing testCategory id', stack: '' })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'ASC', sort: 'id', where: {} }
  const totalCount = await TestCategory.count()
  const rows = await TestCategory.findAll({
    where: params.where,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, 'ASC']
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ testCategories: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await TestCategory.count()
  const rows = await TestCategory.findAll({
    where: params.where,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, 'ASC']
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ testCategories: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
