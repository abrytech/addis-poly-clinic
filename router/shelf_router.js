import { Router } from 'express'
import { Shelf, Patient } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()

router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  Shelf.findOne({ where, include: { model: Patient, as: 'patient' } }).then((shelf) => {
    if (shelf) res.send(shelf)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry Shelf Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  Shelf.create(body).then(async (shelf) => {
    const _shelf = await Shelf.findOne({ where: { id: shelf.id }, include: { model: Patient, as: 'patient' } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_shelf)
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _shelf = await Shelf.findOne({ where: { id: body.id }, include: { model: Patient, as: 'patient' } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_shelf) {
      body.patientId = body.patientId || _shelf.patientId
      body.column = body.column || _shelf.column
      body.row = body.row || _shelf.row
      body.name = body.name || _shelf.name
      body.remark = body.remark || _shelf.remark
      const rows = await Shelf.update(body, { where: { id: body.id } })
      body.patient = _shelf.patient
      res.send({ rows, result: body })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the shelf', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing shelf id', stack: '' })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
  const totalCount = await Shelf.count()
  const rows = await Shelf.findAll({
    where: params.where,
    include: { model: Patient, as: 'patient' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ shelves: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await Shelf.count()
  const rows = await Shelf.findAll({
    where: params.where,
    include: { model: Patient, as: 'patient' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ shelves: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
