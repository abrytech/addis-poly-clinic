import { Router } from 'express'
import { TestType, TestCategory } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()

router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  TestType.findOne({ where, include: { model: TestCategory, as: 'testCategory' } }).then((testType) => {
    if (testType) res.send(testType)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry TestType Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  TestType.create(body).then(async (testType) => {
    const _testType = await TestType.findOne({ where: { id: testType.id }, include: { model: TestCategory, as: 'testCategory' } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_testType)
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _testType = await TestType.findOne({ where: { id: body.id } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_testType) {
      body.testCategoryId = body.testCategoryId || _testType.testCategoryId
      body.name = body.name || _testType.name
      body.normalValue = body.normalValue || _testType.normalValue
      const rows = await TestType.update(body, { where: { id: body.id } })
      const testType = await TestType.findOne({ where: { id: body.id }, include: { model: TestCategory, as: 'testCategory' } }).catch((error) => {
        res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send({ rows, result: testType })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the testType', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing testType id', stack: '' })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'ASC', sort: 'id', where: {} }
  const totalCount = await TestType.count()
  const rows = await TestType.findAll({
    where: params.where,
    include: { model: TestCategory, as: 'testCategory' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, 'ASC']
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ testTypes: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await TestType.count()
  const rows = await TestType.findAll({
    where: params.where,
    include: { model: TestCategory, as: 'testCategory' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, 'ASC']
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ testTypes: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
