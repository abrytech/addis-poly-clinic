import { Router } from 'express'
import visitor from './visitor_router'
import shelf from './shelf_router'
import patient from './patient_router'
import user from './user_router'
import labRequest from './lab_request_router'
import testType from './test_type_router'
import testCategory from './test_category_router'
import labResult from './lab_result_router'
import appointment from './appointment_router'

const router = Router()

router.use('/testCategory', testCategory)
router.use('/testType', testType)
router.use('/labRequests', labRequest)
router.use('/labResults', labResult)
router.use('/visitors', visitor)
router.use('/shelves', shelf)
router.use('/patients', patient)
router.use('/users', user)
router.use('/appointments', appointment)
export default router
