import { compareSync } from 'bcrypt'
import { Router } from 'express'
import { User, Appointment } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()
const include = [{ model: Appointment, as: 'appointments' }]
router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  User.findOne({ where, include }).then((user) => {
    if (user) res.send(user)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry User Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  User.create(body).then(async (user) => {
    const _user = await User.findOne({ where: { id: user.id }, include }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_user)
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('changepassword', async (req, res) => {
  const body = req.body
  if (body.oldPassword && body.newPassword) {
    User.findOne({ where: { id: req.userId }, include }).then(async (user) => {
      if (user) {
        if (compareSync(body.newPassword, user.password)) {
          body.password = body.newPassword
        }
        body.firstName = user.firstName
        body.lastName = user.lastName
        body.username = user.username
        body.phone = user.phone
        body.isActive = user.isActive
        body.role = user.role
        const rows = await User.update(body, { where: { id: body.id } })
        res.send({ rows, result: body })
      } else res.status(400).send({ name: 'Changing password failed', message: 'Changing password failed b/c it couldn\'t find the user', stack: '' })
    }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
  } else res.status(400).send({ name: 'Changing password failed', message: 'Changing password failed, due to missing user id', stack: '' })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _user = await User.findOne({ where: { id: body.id }, include }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_user) {
      body.firstName = body.firstName || _user.firstName
      body.lastName = body.lastName || _user.lastName
      body.username = body.username || _user.username
      body.password = body.password || _user.password
      body.phone = body.phone || _user.phone
      body.isActive = body.isActive || _user.isActive
      body.role = body.role || _user.role
      const rows = await User.update(body, { where: { id: body.id } })
      res.send({ rows, result: body })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the user', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing user id', stack: '' })
})

router.delete('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  User.destroy({ where }).then((rows) => {
    res.send({ rows: rows, message: 'Successfully deleted' })
  }).catch((error) => {
    res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
  const totalCount = await User.count()
  const rows = await User.findAll({
    include,
    where: params.where,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ users: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await User.count()
  const rows = await User.findAll({
    include,
    where: params.where,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ users: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
