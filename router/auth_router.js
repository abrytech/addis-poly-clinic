import { Router } from 'express'
import { User } from '../db/models'
import { compareSync } from 'bcrypt'
import jwt from 'jsonwebtoken'
import { authUser } from '../middleware/auth'
const SECRET_KEY = process.env.ACCESS_TOKEN_SECRET_KEY
const router = Router()

router.get('/me', authUser, async (req, res) => {
  try {
    const where = { id: req.userId }
    User.findOne({
      where
    }).then((user) => {
      if (user) res.send(user)
      else res.status(404).send({ name: 'Resource not found', message: 'User Not Found', stack: '' })
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('/login', async (req, res) => {
  let user = {}
  const { username, password } = req.body
  try {
    if (username && password) {
      user = await User.findOne({ where: { username } })
      if (user) {
        if (compareSync(password, user.password)) {
          if (user.isActive) {
            jwt.sign({ userId: user.id, role: user.role, username: user.username, phone: user.phone }, SECRET_KEY, { expiresIn: '1d' }, (error, token) => {
              if (error) res.status(403).send({ name: error.name, message: error.message, stack: error.stack, expiredAt: error.expiredAt })
              res.set({ Authorization: `Bearer ${token}`, 'Access-control-expose-headers': 'Authorization' }).send(user)
            })
          } else res.status(400).send({ name: 'Authentication Failed', message: 'Oops Sorry currently your account is Disabled', stack: '' })
        } else {
          res.status(400).send({ name: 'Authentication Failed', message: 'Invalid Username or Password', stack: '' })
        }
      }
    }
  } catch (error) {
    res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('/register', async (req, res) => {
  try {
    const body = req.body
    console.info(body)
    const _user = await User.create(body)
    res.send(_user)
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

export default router
