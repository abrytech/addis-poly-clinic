import { Router } from 'express'
import { Appointment, Patient, User } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()
const include = [{ model: Patient, as: 'patient' }, { model: User, as: 'assignedDoctor' }, { model: User, as: 'creator' }, { model: User, as: 'updater' }]
router.get('/:id(\\d+)', async (req, res) => {
  try {
    const where = { id: req.params.id }
    Appointment.findOne({ where, include }).then((appointment) => {
      if (appointment) res.send(appointment)
      else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry Appointment Not Found', stack: '' })
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('/bulk', async (req, res, next) => {
  try {
    let bodies = req.body
    if (Array.isArray(bodies)) {
      bodies = bodies.map((body) => {
        body.createdBy = req.userId
        return body
      })
    }
    console.log('bodies: ', bodies)
    Appointment.bulkCreate(bodies).then(async (appointment) => {
      res.send(appointment)
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('', async (req, res, next) => {
  try {
    const body = req.body
    body.createdBy = req.userId
    Appointment.create(body).then(async (appointment) => {
      const _appointment = await Appointment.findOne({ where: { id: appointment.id }, include }).catch((error) => {
        res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send(_appointment)
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _appointment = await Appointment.findOne({ where: { id: body.id } }).catch((error) => {
      res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_appointment) {
      body.patientId = body.patientId || _appointment.patientId
      body.doctorId = body.doctorId || _appointment.doctorId
      body.remark = body.remark || _appointment.remark
      body.appointmentDate = body.appointmentDate || _appointment.appointmentDate
      body.updatedBy = req.userId
      const rows = await Appointment.update(body, { where: { id: body.id } })
      const appointment = await Appointment.findOne({ where: { id: body.id }, include }).catch((error) => {
        res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send({ rows, result: appointment })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the appointment', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing appointment id', stack: '' })
})

router.get('', async (req, res) => {
  try {
    const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
    const totalCount = await Appointment.count()
    const rows = await Appointment.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    res.send({ appointments: rows, page: params.page, count: rows.length, total: totalCount })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.get('/:query', getParams, async (req, res) => {
  try {
    const params = req.queries
    const totalCount = await Appointment.count()
    const rows = await Appointment.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    res.send({ appointments: rows, page: params.page, count: rows.length, total: totalCount })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

export default router
