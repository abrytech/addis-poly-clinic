import { Router } from 'express'
import { Visitor, Patient } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()

router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  Visitor.findOne({ where, include: { model: Patient, as: 'patient' } }).then((visitor) => {
    if (visitor) res.send(visitor)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry Visitor Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  Visitor.create(body).then(async (visitor) => {
    const _visitor = await Visitor.findOne({ where: { id: visitor.id } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_visitor)
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _visitor = await Visitor.findOne({ where: { id: body.id }, include: { model: Patient, as: 'patient' } }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_visitor) {
      body.patientId = body.patientId || _visitor.patientId
      body.date = body.date || _visitor.date
      const rows = await Visitor.update(body, { where: { id: body.id } })
      body.patient = _visitor.patient
      res.send({ rows, result: body })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the visitor', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing visitor id', stack: '' })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
  const totalCount = await Visitor.count()
  const rows = await Visitor.findAll({
    where: params.where,
    include: { model: Patient, as: 'patient' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ visitors: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await Visitor.count()
  const rows = await Visitor.findAll({
    where: params.where,
    include: { model: Patient, as: 'patient' },
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ visitors: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
