import { Router } from 'express'
import { TestType, TestCategory } from '../db/models'
const router = Router()
const _categories = [
  { name: 'HematolOgy', remark: '' },
  { name: 'Urin Analysis', remark: '' },
  { name: 'Chemistry', remark: '' },
  { name: 'Serology & immuno-Hematology', remark: '' },
  { name: 'Parasitology', remark: '' },
  { name: 'Stool Test', remark: '' },
  { name: 'Direct Microscopy', remark: '' },
  { name: 'Bacteriology', remark: '' }
]

const _types = [
  { name: 'CBC', testCategoryId: 1, normalValue: '' },
  { name: 'WBC', testCategoryId: 1, normalValue: '' },
  { name: 'DIFF (N, E, B)', testCategoryId: 1, normalValue: '' },
  { name: 'HGB', testCategoryId: 1, normalValue: '' },
  { name: 'HCT', testCategoryId: 1, normalValue: '' },
  { name: 'ESR', testCategoryId: 1, normalValue: '' },
  { name: 'RBC', testCategoryId: 1, normalValue: '' },
  { name: 'PLATELET', testCategoryId: 1, normalValue: '' },
  { name: 'OTHERS', testCategoryId: 1, normalValue: '' },

  { name: 'COLOUR', testCategoryId: 2, normalValue: '' },
  { name: 'ASPECT', testCategoryId: 2, normalValue: '' },
  { name: 'SP.GR.', testCategoryId: 2, normalValue: '' },
  { name: 'PH', testCategoryId: 2, normalValue: '' },
  { name: 'ALBUMN', testCategoryId: 2, normalValue: '' },
  { name: 'GLUCOSE', testCategoryId: 2, normalValue: '' },
  { name: 'KETONE', testCategoryId: 2, normalValue: '' },
  { name: 'NITRITE', testCategoryId: 2, normalValue: '' },
  { name: 'BILIRUBIN', testCategoryId: 2, normalValue: '' },
  { name: 'UROBILINOGEN', testCategoryId: 2, normalValue: '' },
  { name: 'BLOOD', testCategoryId: 2, normalValue: '' },
  { name: 'LEUKOCYTES', testCategoryId: 2, normalValue: '' },
  { name: 'MIC', testCategoryId: 2, normalValue: '' },
  { name: 'EP. CELLS', testCategoryId: 2, normalValue: '' },
  { name: 'RBC', testCategoryId: 2, normalValue: '' },
  { name: 'WBC', testCategoryId: 2, normalValue: '' },
  { name: 'CASTS', testCategoryId: 2, normalValue: '' },
  { name: 'OTHERS', testCategoryId: 2, normalValue: '' },

  { name: 'FBS', testCategoryId: 3, normalValue: '' },
  { name: 'RBS', testCategoryId: 3, normalValue: '' },
  { name: 'SGOT', testCategoryId: 3, normalValue: '' },
  { name: 'SGPT', testCategoryId: 3, normalValue: '' },
  { name: 'ALK.PHOS.', testCategoryId: 3, normalValue: '' },
  { name: 'D.BILIRUBIN', testCategoryId: 3, normalValue: '' },
  { name: 'T.BILIRUBIN', testCategoryId: 3, normalValue: '' },
  { name: 'ACID.PHOS', testCategoryId: 3, normalValue: '' },
  { name: 'BUN', testCategoryId: 3, normalValue: '' },
  { name: 'CREATININE', testCategoryId: 3, normalValue: '' },
  { name: 'URIC ACID', testCategoryId: 3, normalValue: '' },
  { name: 'T.PROTIEN', testCategoryId: 3, normalValue: '' },
  { name: 'TRIGLYCERIDNS', testCategoryId: 3, normalValue: '' },
  { name: 'CHOLESTEROL', testCategoryId: 3, normalValue: '' },
  { name: 'HDL', testCategoryId: 3, normalValue: '' },
  { name: 'LDL', testCategoryId: 3, normalValue: '' },
  { name: 'LDH', testCategoryId: 3, normalValue: '' },
  { name: 'SODIUM', testCategoryId: 3, normalValue: '' },
  { name: 'POTASSIUM', testCategoryId: 3, normalValue: '' },
  { name: 'CHIORIDE', testCategoryId: 3, normalValue: '' },
  { name: 'CALCIUM', testCategoryId: 3, normalValue: '' },
  { name: 'OTHERS', testCategoryId: 3, normalValue: '' },
  { name: 'PT', testCategoryId: 3, normalValue: '' },
  { name: 'PPT', testCategoryId: 3, normalValue: '' },
  { name: 'INR', testCategoryId: 3, normalValue: '' },

  { name: 'VDRL', testCategoryId: 4, normalValue: '' },
  { name: 'TPHA', testCategoryId: 4, normalValue: '' },
  { name: 'WWF (SH, SO, OX19))', testCategoryId: 4, normalValue: '' },
  { name: 'BLOOD GROUP & RH)', testCategoryId: 4, normalValue: '' },
  { name: 'HBSAG)', testCategoryId: 4, normalValue: '' },
  { name: 'Anti-HCV)', testCategoryId: 4, normalValue: '' },
  { name: 'RF)', testCategoryId: 4, normalValue: '' },
  { name: 'ASO TITER)', testCategoryId: 4, normalValue: '' },
  { name: 'TROPONIN (QUALITATIVE)', testCategoryId: 4, normalValue: '' },
  { name: 'URINE HCG', testCategoryId: 4, normalValue: '' },
  { name: 'CRP (QUALITATIVE)', testCategoryId: 4, normalValue: '' },

  { name: 'PARASITOLOGY', testCategoryId: 5, normalValue: '' },

  { name: 'APPEARANCE', testCategoryId: 6, normalValue: '' },
  { name: 'CONSISTANCY', testCategoryId: 6, normalValue: '' },

  { name: 'OCCULT BLOOD', testCategoryId: 7, normalValue: '' },
  { name: 'B/F FOR H/P', testCategoryId: 7, normalValue: '' },
  { name: 'OTHERS', testCategoryId: 7, normalValue: '' },

  { name: 'WET MOUNT', testCategoryId: 8, normalValue: '' },
  { name: 'KOH', testCategoryId: 8, normalValue: '' },
  { name: 'GRAMSTAIN', testCategoryId: 8, normalValue: '' },
  { name: 'SPACTUM (AFB)', testCategoryId: 8, normalValue: '' },
  { name: 'H.PYLORI AG', testCategoryId: 8, normalValue: '' },
  { name: 'H.POILORY AB', testCategoryId: 8, normalValue: '' }
]
router.get('/', async (req, res) => {
  try {
    const result = { testTypes: [], testCategories: [], skipedTestTypes: [], skipedTestCategories: [] }
    const typesCount = await TestType.count()
    // const allTypes = await TestType.findAll()
    const categoriesCount = await TestCategory.count()
    // const allCategories = await TestCategory.findAll() || []
    console.log('typesCount: ', typesCount, 'categoriesCount:', categoriesCount)
    if (categoriesCount === 0) {
      result.testCategories = await TestCategory.bulkCreate(_categories)
    } else result.skipedTestCategories = _categories
    if (typesCount === 0) {
      result.testTypes = await TestType.bulkCreate(_types)
    } else result.skipedTestTypes = _types
    // if (allCategories.length !== _categories.length) {
    //   for (const category in _categories) {
    //     // const count = await TestCategory.count({ where: category })
    //     const cats = allCategories.filter((cat) => cat.name !== category.name && cat.remark !== category.remark)
    //     if (cats.length) {
    //       const cat = await TestCategory.bulkCreate(cats).catch((err) => {
    //         result.skipedTestCategories.push(category)
    //         console.log(err)
    //       })
    //       result.testCategories.push(cat)
    //     } else result.skipedTestCategories.push(category)
    //   }
    // }
    // if (typesCount !== _types.length) {
    //   _types.forEach(async type => {
    //     const count = await TestType.count({ where: type })
    //     if (!count) {
    //       const typ = await TestType.create(type).catch((err) => {
    //         result.skipedTestTypes.push(type)
    //         console.log(err)
    //       })
    //       result.testTypes.push(typ)
    //     }
    //   })
    // }
    res.send(result)
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

export default router
