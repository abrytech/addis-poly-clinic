import { Router } from 'express'
import { Lab, Visitor, Shelf, Patient, User } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()
const include = [{ model: Lab, as: 'labs' }, { model: Visitor, as: 'visits' }, { model: Shelf, as: 'shelf' }, { model: User, as: 'creator' }, { model: User, as: 'updater' }]
router.get('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  Patient.findOne({ where, include }).then((patient) => {
    if (patient) res.send(patient)
    else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry Patient Not Found', stack: '' })
  }).catch((error) => {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.post('', async (req, res, next) => {
  const body = req.body
  body.createdBy = req.userId
  // console.log('!body.cardNumber', !body.cardNumber)
  if (!body.cardNumber) {
    let _cardNumber = 'APC'
    const lastId = await Patient.max('id')
    console.log('lastId:', lastId)
    if (lastId) {
      const newId = lastId + 1
      if (lastId >= 1000) _cardNumber = `${_cardNumber}-${newId}`
      if (lastId < 1000 && lastId > 100) _cardNumber = `${_cardNumber}-0${newId}`
      if (lastId < 100 && lastId > 10) _cardNumber = `${_cardNumber}-00${newId}`
      if (lastId < 10) _cardNumber = `${_cardNumber}-000${newId}`
    } else {
      _cardNumber = `${_cardNumber}-0001`
    }
    body.cardNumber = _cardNumber
  }
  Patient.create(body).then(async (patient) => {
    const _patient = await Patient.findOne({ where: { id: patient.id }, include }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    res.send(_patient)
  }).catch((error) => {
    res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _patient = await Patient.findOne({ where: { id: body.id }, include }).catch((error) => {
      res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_patient) {
      body.firstName = body.firstName || _patient.firstName
      body.fatherName = body.fatherName || _patient.fatherName
      body.grandName = body.grandName || _patient.grandName
      body.gender = body.gender || _patient.gender
      body.age = body.age || _patient.age
      body.phone = body.phone || _patient.phone
      body.kebele = body.kebele || _patient.kebele
      body.woreda = body.woreda || _patient.woreda
      body.subCity = body.subCity || _patient.subCity
      body.houseNo = body.houseNo || _patient.houseNo
      body.updatedBy = req.userId || _patient.updatedBy
      const rows = await Patient.update(body, { where: { id: body.id } })
      if (rows) {
        const patient = await Patient.findOne({ where: { id: body.id }, include }).catch((error) => {
          res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
        })
        res.send({ rows, result: patient })
      } else {
        res.send({ rows, result: _patient })
      }
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the patient', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing patient id', stack: '' })
})

router.delete('/:id(\\d+)', async (req, res) => {
  const where = { id: req.params.id }
  Patient.destroy({ where }).then((rows) => {
    res.send({ rows: rows, message: 'Successfully deleted' })
  }).catch((error) => {
    res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
  })
})

router.get('', async (req, res) => {
  const params = { page: 1, limit: 25, order: 'DESC', sort: 'id' }
  const totalCount = await Patient.count()
  const rows = await Patient.findAll({
    include,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ patients: rows, page: params.page, count: rows.length, total: totalCount })
})

router.get('/:query', getParams, async (req, res) => {
  const params = req.queries
  const totalCount = await Patient.count()
  const rows = await Patient.findAll({
    where: params.where,
    include,
    offset: (params.page - 1) * params.limit,
    limit: params.limit,
    order: [
      [params.sort, params.order]
    ]
  }).catch((error) => {
    res.send({ name: error.name, message: error.message, stack: error.stack })
  })
  res.send({ patients: rows, page: params.page, count: rows.length, total: totalCount })
})

export default router
