import { Router } from 'express'
import { LabResult, Patient, User, TestType } from '../db/models'
import { getParams } from '../middleware/auth'
const router = Router()
const include = [{ model: Patient, as: 'patient' }, { model: User, as: 'laboratorist' }, { model: User, as: 'updater' }, { model: TestType, as: 'testType' }]
router.get('/:id(\\d+)', async (req, res) => {
  try {
    const where = { id: req.params.id }
    LabResult.findOne({ where, include }).then((labResult) => {
      if (labResult) res.send(labResult)
      else res.status(404).send({ name: 'resource not found', message: 'Oops Sorry LabResult Not Found', stack: '' })
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('/bulk', async (req, res, next) => {
  try {
    let bodies = req.body
    if (Array.isArray(bodies)) {
      bodies = bodies.map((body) => {
        body.createdBy = req.userId
        return body
      })
    }
    console.log('bodies: ', bodies)
    LabResult.bulkCreate(bodies).then(async (labResult) => {
      res.send(labResult)
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.post('', async (req, res, next) => {
  try {
    const body = req.body
    body.createdBy = req.userId
    LabResult.create(body).then(async (labResult) => {
      const _labResult = await LabResult.findOne({ where: { id: labResult.id }, include }).catch((error) => {
        res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send(_labResult)
    })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.put('', async (req, res) => {
  const body = req.body
  if (body.id) {
    const _labResult = await LabResult.findOne({ where: { id: body.id } }).catch((error) => {
      res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
    })
    if (_labResult) {
      body.patientId = body.patientId || _labResult.patientId
      body.testTypeId = body.testTypeId || _labResult.testTypeId
      body.labRequestId = body.labRequestId || _labResult.labRequestId
      body.result = body.result || _labResult.result
      body.remark = body.remark || _labResult.remark
      body.updatedBy = req.userId
      const rows = await LabResult.update(body, { where: { id: body.id } })
      const labResult = await LabResult.findOne({ where: { id: body.id }, include }).catch((error) => {
        res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
      })
      res.send({ rows, result: labResult })
    } else res.status(400).send({ name: 'Update failed', message: 'update failed b/c it couldn\'t find the labResult', stack: '' })
  } else res.status(400).send({ name: 'Update failed', message: 'update failed, due to missing labResult id', stack: '' })
})

router.get('', async (req, res) => {
  try {
    const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
    const totalCount = await LabResult.count()
    const rows = await LabResult.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    res.send({ labResults: rows, page: params.page, count: rows.length, total: totalCount })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

router.get('/:query', getParams, async (req, res) => {
  try {
    const params = req.queries
    const totalCount = await LabResult.count()
    const rows = await LabResult.findAll({
      where: params.where,
      include,
      offset: (params.page - 1) * params.limit,
      limit: params.limit,
      order: [
        [params.sort, params.order]
      ]
    })
    res.send({ labResults: rows, page: params.page, count: rows.length, total: totalCount })
  } catch (error) {
    res.status(500).send({ name: error.name, message: error.message, stack: error.stack })
  }
})

export default router
