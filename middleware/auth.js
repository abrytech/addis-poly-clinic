import { verify } from 'jsonwebtoken'
import { Op } from 'sequelize'
import { User } from '../db/models'
const { ACCESS_TOKEN_SECRET_KEY } = process.env
const error = Error('Your not an authorized user, please singIn first')
error.name = '401 Unauthorized'
error.status = 401

const authUser = async (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (authHeader) {
    // console.log(`::::::::::::::::::<<<<<<<<<<<<<<< typeof (authHeader): ${typeof (authHeader)} >>>>>>>>>>>:::::::::::::::::`)
    // console.log(`::::::::::::::::::<<<<<<<<<<<<<<< authHeader: ${authHeader} >>>>>>>>>>>:::::::::::::::::`)
    // console.log('2nd typeof authHeader', typeof authHeader)
    const token = `${authHeader}`.split(' ')[1]
    // console.log('3rd $token', token)
    if (!token || token === '') next(error)
    // console.log('{!req.userId} || {!req.role} || {!req.username}', `${req.userId} || ${req.role} || ${req.username}`)
    if (!req.userId || !req.role || !req.username) {
      verify(token, ACCESS_TOKEN_SECRET_KEY, async (err, decoded) => {
        if (err) {
          error.message = err.message
          error.name = err.name
          next(error)
        } else {
          const where = { id: decoded.userId, role: decoded.role, username: decoded.username, isActive: true }
          User.findOne({ where }).then((user) => {
            if (user) {
              req.userId = user.id
              req.role = user.role
              req.username = user.username
              req.phone = user.phone
              next()
            } else {
              error.message = 'Invalid token,  Please signin again your access token may be expired'
              next(error)
            }
          }).catch((err) => {
            error.message = err.message
            error.stack = err.stack
            next(error)
          })
        }
      })
    } else {
      const where = { id: req.userId, role: req.role, username: req.username }
      const isUser = await User.findOne({ where }).catch((err) => {
        error.message = err.message
        error.stack = err.stack
        next(error)
      })
      if (isUser) next()
      else {
        error.message = 'Invalid token,  Please signin again your access token may be expired'
        next(error)
      }
      next()
    }
  } else next(error)
}

const getParams = (req, res, next) => {
  const params = { page: 1, limit: 25, order: 'DESC', sort: 'id', where: {} }
  let query = req.params.query || ''
  try {
    const expString = '[?]{1}[a-zA-Z0-9%&=@.]+[a-zA-Z0-9]{1,}|[a-zA-Z0-9%&=@.]+[a-zA-Z0-9]{1,}'
    const isQueryValid = new RegExp(expString).test(query)
    // console.info('req.params.query', query, 'isQueryValid', isQueryValid)
    if (isQueryValid) {
      query = query.startsWith('?') ? query.substring(1) : query
      const temp = query.split('&')
      temp.forEach((param) => {
        const key = param.split('=')[0]
        const value = param.split('=')[1]
        console.log("key, value", key, value);
        if (key && value) {
          if (key === 'page' || key === 'limit') {
            params[key] = parseInt(value)
          }
          else if (key === 'order' || key === 'sort') {
            params[key] = value
          }
          else if (key === 'createdAt') params.where[key] = { [Op.startsWith]: value }
          else if (key === 'updatedAt') params.where[key] = { [Op.startsWith]: value }
          else if (key === 'firstName') params.where[key] = { [Op.startsWith]: value }
          else if (key === 'fatherName') params.where[key] = { [Op.startsWith]: value }
          else if (key === 'grandName') params.where[key] = { [Op.startsWith]: value }
          else if (key === 'cardNumber') params.where[key] = { [Op.substring]: value }
          else params.where[key] = value
          console.log("Param", params.where)
        }
      })
      req.queries = params
      next()
    } else throw Error('Bad Parameter, Invalid Request URL Parameter')
  } catch (error) {
    res.status(400).send({ name: error.name, message: error.message, stack: error.stack })
  }
}

export { authUser, getParams }
